help: Show all commands.
project-create: Create new project
project-list: Show all projects
project-remove: Remove selected project by name
project-clear: Remove all projects
project-show-task: Show all task in the selected project
project-select: Select project by name
task-select: Select task by name
task-stick: Stick task into selected project
task-create: Create new task
task-list: Show all tasks
task-remove: Remove selected task by name
task-clear: Remove all tasks in the selected project
task-clear-all: Remove all task
clear-selection: Clear selected project and task
exit: Exit from the Task Manager

