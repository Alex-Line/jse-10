package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Status;
import com.iteco.linealex.jse.util.DateFormatter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public final class Project extends AbstractEntity implements Serializable {

    @Nullable
    private String name = "unnamed project";

    @Nullable
    private String description = "";

    @Nullable
    private Date dateStart = new Date();

    @Nullable
    private Date dateFinish = new Date();

    @Nullable
    private String userId = null;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + super.getId() +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + DateFormatter.formatDateToString(dateStart) +
                ",\n    Finish Date = " + DateFormatter.formatDateToString(dateFinish) +
                ",\n    Status = " + status.getName() +
                ",\n    User ID = " + userId +
                " }";
    }

}