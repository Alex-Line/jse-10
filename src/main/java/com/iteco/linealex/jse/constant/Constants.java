package com.iteco.linealex.jse.constant;

import java.io.File;

public class Constants {

    public static final String SAVE_DIR = System.getProperty("user.dir") + File.separator
            + "target" + File.separator + "taskmanager" + File.separator + "data" + File.separator;

}
