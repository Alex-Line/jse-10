package com.iteco.linealex.jse.dto;

import com.iteco.linealex.jse.api.service.ServiceLocator;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"users", "projects", "tasks"}, name = "Domain")
public class Domain implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(required = true, name = "users")
    private List<User> users;

    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(required = true, name = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(required = true, name = "tasks")
    private List<Task> tasks;

    public void load(ServiceLocator serviceLocator) {
        users = new LinkedList<>(serviceLocator.getUserService().getAllEntities());
        projects = new LinkedList<>(serviceLocator.getProjectService().getAllEntities());
        tasks = new LinkedList<>(serviceLocator.getTaskService().getAllEntities());
    }

}